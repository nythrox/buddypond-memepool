# Buddy Pond MemePool

This repository contains a collection of memes used at Buddypond.com

Feel free to make a PR to add your own.

# Mnemosyne

*Mnemosyne is the goddess of memory and the mother of the nine Muses.*

![Mnemosyne](https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Mosaïque_murale_Mnémosyne.jpg/440px-Mosaïque_murale_Mnémosyne.jpg)

`Mnemosyne` is used to query the Memepool and return the most excellant results based on: Levenstein Distance, Jaro Winkler Values, plus  some custom search algos.

You may call `Mnemosyne.query(searchString)` and get back an array of Meme results which will have relative context based on your query.

## Examples

See `./examples` for code examples

`node examples/query-memepool.js`


```

{
  author: 'Mnemosyne',
  type: 'image',
  filename: 'dance-learn-donkey-roll-styleboys.gif',
  levenshtein: 29,
  winkler: '0.427'
}
```