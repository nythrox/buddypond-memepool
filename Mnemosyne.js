let Mnemosyne = {};
let path = require('path');
let fs = require('fs');

const levenshtein = require('./vendor/levenshtein');
const winkler = require('./vendor/winkler');

let memePath = path.resolve(__dirname + "/memes/");

let clientPath = path.resolve(__dirname + "/memes.js");

Mnemosyne.load = function loadMemes () {
  let files = fs.readdirSync(memePath);

  // TODO: use .gitignore file instead of hard-coded array here
  let ignored = ['.DS_Store']
  files = files.filter(function(f){
    if (ignored.indexOf(f) === -1) {
      return true;
    }
    return false;
  });
  // write these files to the public buddypond-client path
  let str = JSON.stringify(files, true, 2);
  str = 'let Memepool = ' + str + ';';
  fs.writeFileSync(clientPath, str);
  return files;
}

// console.log('memePath', memePath);
Mnemosyne.query = function (str) {

  // let str = 'future is now old man';
  let results = [];

  let searchSet = [];

  let directMatches = 0;

  // lots of memes to parse through
  // first, let's narrow down the list for exact word matches
  // most likely, we should find a few. use this list for further processing
  // if no results were returned, use the entire lisf fur further processing
  let words = str.split(' ');
  Mnemosyne.keys.forEach(function(m){
    let found = false;
    words.forEach(function(word){
      if (m.search(word) !== -1 && searchSet.indexOf(m) === -1) {
        searchSet.push(m);
      }
    });
  })

  directMatches = searchSet.length;
  // console.log('searchSet', searchSet)
  if (directMatches < 5) {
    searchSet = Mnemosyne.keys;
  }

  searchSet.forEach(function(m){
    let filename = m.split('.')[0];
    let lDistance = levenshtein(str, filename);
    let wDistance = winkler.distance(str, filename);
    results.push({ filename: m, levenshtein: lDistance, winkler: wDistance });
  });

  if (results.length > 20 && directMatches < 5) {
    results = results.filter(function(a){
      if (a.winkler < 0.5555) {
        return false
      }
      return true;
    });
  } 

  results = results.sort(function(a, b){
    if (a.winkler > b.winkler) {
      return 1;
    }
    if (b.winkler > a.winkler) {
      return -1;
    }
    return 0;
  
  });

  // console.log('query results:', results.length);

  results.reverse();
  //  results = results.slice(0, 20);

  /*
  results.forEach(function(m){
    console.log(m.winkler, m.levenshtein, m.filename)
  });
  */

  let randomImage = results[Math.floor(Math.random() * results.length)];
  if (!randomImage) {
    return false;
  }

  let imageTitle = randomImage.filename.replace(/-/g, ' ');
  imageTitle = imageTitle.split('.')[0];

  let card = {
    'author': 'Mnemosyne',
    'type': 'image',
    'filename': randomImage.filename,
    'title': imageTitle,
    'levenshtein': randomImage.levenshtein,
    'winkler': randomImage.winkler.toFixed(3)
  };
  // console.log('returning', card)
  return card;
}

Mnemosyne.keys = Mnemosyne.load();

//console.log(memes.keys)

module.exports = Mnemosyne;